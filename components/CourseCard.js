import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import AppHelper from '../app-helper';
import Swal from 'sweetalert2'

export default function CourseCard({ courseProp }){
	const { name, description, price, start_date, end_date } = courseProp;
	const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(20);
	const [isOpen, setIsOpen] = useState(true);

	function enroll(courseId){
		console.log(courseId)
		fetch(`${ AppHelper.API_URL }/users/enroll`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				courseId: courseId,
				googleToken: localStorage.getItem('googleToken')
			})
		})
		.then(res => {
        	return res.json()
        })
		.then(data => {
			console.log(data)
			Swal.fire({
				icon: 'success',
				title: 'Enrolled Successfully!',
				text: 'You will be receiving an Email message for verification'
			})
		})
	}


	useEffect(() => {
		if(seats === 0){
			setIsOpen(false);
		}
	}, [seats])


	return(
          <Card>
            <Card.Body>
              <Card.Title>{name}</Card.Title>
              <Card.Text>         
              <span className="subtitle"> Description:  </span>
              {description}
              <br />
              <span className="subtitle"> Price:  </span>
              PHP {price}
              <br />
              <span className="subtitle"> Start Date:   </span>
              {start_date}
              <br />
              <span className="subtitle"> End Date : </span>
              {end_date}
              <br />
              </Card.Text>
              {isOpen ?
                <Button className="bg-primary" onClick={() => {enroll(courseProp._id)}}>Enroll</Button>
                : 
                <Button className="bg-primary" disabled>Not Available</Button>
              }                         
            </Card.Body> 
          </Card>   
    )

}

CourseCard.propTypes = {
	courseProp: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description:PropTypes.string.isRequired,
		price:PropTypes.number.isRequired
	})
}